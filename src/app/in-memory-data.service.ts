import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService {
  createDb() {
    const heroes = [
      { id: 11, name: 'Phantom Assassin' },
      { id: 12, name: 'Pudge' },
      { id: 13, name: 'Shadow Fiend' },
      { id: 14, name: 'Invoker' },
      { id: 15, name: 'Luna' },
      { id: 16, name: 'Storm Spirit' },
      { id: 17, name: 'Earth Spirit' },
      { id: 18, name: 'Earth Shaker' },
      { id: 19, name: 'Sniper' },
      { id: 20, name: 'Jugernaut' }
    ];
    return {heroes};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
  constructor() { }
}
