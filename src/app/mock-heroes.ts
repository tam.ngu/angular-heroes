import { Hero } from './hero';

export const HEROES: Hero[] = [
    { id: 11, name: 'Phantom Assassin' },
    { id: 12, name: 'Pudge' },
    { id: 13, name: 'Shadow Fiend' },
    { id: 14, name: 'Invoker' },
    { id: 15, name: 'Luna' },
    { id: 16, name: 'Storm Spirit' },
    { id: 17, name: 'Earth Spirit' },
    { id: 18, name: 'Earth Shaker' },
    { id: 19, name: 'Sniper' },
    { id: 20, name: 'Jugernaut' }
];